import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


public class PodsGeneration
{
	// for now, the code assumes a CSV file of questions and keywords as input, along with a CSV file of the 
	// StanfordCoreNLP output. This will probably need to e changed to handle test questions on a per-question basis 
	static Map<String, ArrayList<Word>> keywords_map = new HashMap<String, ArrayList<Word>>();
	static Map<String, ArrayList<Word>> words_map = new HashMap<String, ArrayList<Word>>();
	static Map<String, ArrayList<ArrayList<PODNode>>> PODS_map = new HashMap<String, ArrayList<ArrayList<PODNode>>>();
	// Read in the CSV files and create dicts
	Question[] questions = new Question[100];
	
	
	public static ArrayList<PODNode> nonboolean_rules(String q)
	{
		/*
		 * detect named entities : if capitalized (except if JJ) or if stanfordcoreNLP output (if 2 NEs identified are adjacent, clump together)
				if 1 NE, that is pivot
				if 2 NE, then the one with more relations is the pivot
				if 0 NE, then if 1 noun, that is pivot
				    else if 2 nouns

			Once pivot identified :
			    identify all 'A of B' phrases using the keywords, order as B->A (group together as nodes if required)
			
			    the word immediately after 'Which' should be last in the PODS order
			
			    if 2 NEs, once pivot identified, in the PODS, 2 NEs should not be adjacent
		 */
		ArrayList<PODNode> pods_for_this_q = new ArrayList<PODNode>();
		
		// getting pivot for each question 
		
			  
			  ArrayList<Word> value = words_map.get(q.trim());
			  ArrayList<Word> candidate_pivots = new ArrayList<Word>();
			  ArrayList<PODNode> pods = new ArrayList<PODNode>();
			  
			  // all NEs (or nouns if no NES) will be candidate pivots - detect this from the word tags
			  for (Word w: value)
			  {
				  if (!w.NETag.equals("O") && !w.NETag.equals("NUMBER") && !w.NETag.equals("DATE"))
				  {
					  candidate_pivots.add(w);
				  }
			  }
			  
			  if (candidate_pivots.size()==0) // this means we have no NEs in the question. In this case, choose pivot from nouns
			  {
				  for (Word w: value)
				  {
					  if (w.POSTag.startsWith("N"))
					  {
						  candidate_pivots.add(w);
					  }
				  }
			  }
			  if (candidate_pivots.size()==0) // this means we have no nouns in the question either. In this case, we are fucked (not doing error handling for this in the code that follows this coz this is extremely unlikely)
			  {
				  System.out.println("No candidate pivots detected. Now what. (This is extremely unlikely. If you see this, you have a very weird question)");
			  }
			  
			  System.out.println(q);
			 
			  // Map detected candidate pivot words to corresponding keywords from the keyword map
			  ArrayList<Word> keywords = keywords_map.get(q.trim());
			  
			  ArrayList<Word> final_pivots = new ArrayList<Word>();
			  
			  // (this is probably a terribly inefficient piece of code...)
			  
			  try
			  {
			  for (Word candidate : candidate_pivots)
			  {
				  
				  for (Word ww:keywords)
				  {
					  if(ww.text.contains(candidate.text) || candidate.text.contains(ww.text) || ww.text.contains(candidate.lemma) )
					  {
						  System.out.println(candidate.text);
						  System.out.println(candidate.NETag);
						  System.out.println(candidate.POSTag);
						  String trimmed = candidate.text.trim();
						  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
						  if (words>1)
							  ww.lemma = candidate.lemma;
						  else
							  ww.lemma = ww.text;
						  ww.POSTag = candidate.POSTag;
						  final_pivots.add(ww);
						  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
						  System.out.println(ww.text);
						  System.out.println("----");
					  }
					  
				  }
			  }
			  
			  
			  
			  
			  // printing candidates to check...
			  // removing duplicates -
			  ArrayList<Word> all_candidate_pivots = new ArrayList<Word>(new LinkedHashSet<Word>(final_pivots));
			  Word actual_pivot = DBPediaInterface.getPivot(all_candidate_pivots);
			  //Word actual_pivot = all_candidate_pivots.get(0);
			  for (Word candidate : all_candidate_pivots) // contains candidate pivots for this question. What if this is empty??
			  {
				  System.out.println("Final pivot - " +candidate.text);
				  System.out.println("*********************************");
			  }
			  
			  // remove actual_pivot from keywords list
			  // keywords list will now contain remaining keywords

			  Iterator<Word> it = keywords.iterator();
			  while (it.hasNext()) {
			      Word w = it.next();
			      if (w.text.equals(actual_pivot.text)) {
			      it.remove();
			      }
			  }
			  
			  
			  PODNode pod = new PODNode(actual_pivot.text, actual_pivot.POSTag, actual_pivot.lemma);
			  pods.add(pod); // create PODNode and add
			  System.out.println("Added pivot");
			  
			// which .. rule
			// find which keyword follows 'which'
			// if 'which' in question, then find next word matching with keyword
			  int j=-1, which_index=-1;
			  Word Which = new Word("Which", "", "", "");
			  Word which = new Word("which", "", "", "");
			  for (Word w : value)
			  {		j++;
				  if (w.equals(Which)||w.equals(which))
				  {
					  which_index=j;  
					  System.out.println("Which found!!");
					  break;
				  }
					  
			  }
			  
			  Word which_word=null; // keep track of this
			  if (which_index>=0)
				
			  {	  System.out.println("the which word ------ >>");
				  //find next keyword
			  int flag =0;
				  for (int i = which_index+1; i < value.size(); i++)
				  {   
					  Word this_word = value.get(i);
					  for (Word ww:keywords)
					  {
						  if(ww.text.contains(this_word.text) || this_word.text.contains(ww.text) || ww.text.contains(this_word.lemma) )
						  {
							  System.out.println(this_word.text);
							  System.out.println(this_word.NETag);
							  System.out.println(this_word.POSTag);
							  String trimmed = this_word.text.trim();
							  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
							  if (words>1)
								  ww.lemma = this_word.lemma;
							  else
								  ww.lemma = ww.text;
							  ww.POSTag = this_word.POSTag;
							  which_word = ww;
							  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
							  System.out.println(ww.text);
							  System.out.println("----~~~~");
							  flag=1;
							  break;
						  }
						  
					  }
					  if (flag==1)
					  {break;
					  }
					  
				  }
			  }
			  System.out.println("Found which");
			  if (which_word!=null) // remove from keywords list
			  {	  System.out.println(which_word.text);
				  Iterator<Word> its = keywords.iterator();
				  while (its.hasNext()) {
				      Word w = its.next();
				      if (w.text.equals(which_word.text)) {
				      its.remove();
				      }
				  }
			  }
			  System.out.println("Removed which from keywords");
			// of .. of rule
			// if 'of' in question, split question at that point, find nearest keywords in both directions
			// repeat if necessary
			  
			  int flag =0;
			  Word of = new Word("of", "", "", "");
			  j=-1; int of_index_1=-1;
			  for (Word w : value)
			  {		j++;
				  if (w.equals(of))
				  {
					  of_index_1=j;  
					  System.out.println("Of found!!");
					  break;
				  }
					  
			  }
			  // first occurrence of 'of'
			  int of_index_2 = -1;
			  for (int i = of_index_1+1; i < value.size(); i++)
			  {
				  if(value.get(i).equals(of))
				  {
					  of_index_2 = i;	// for now, assume only 2 occurrences of 'of' in the question
					  System.out.println("2nd of found!!");
				  }
					  
			  }
			  
			  Word of_1=null;
			  Word of_2=null;
			  Word of_3=null;
			  Word of_4=null;
			  
			  if(of_index_1>-1)
			  {
				  if(of_index_2>-1)
				  {
				  for (int i = of_index_2+1; i < value.size(); i++)
				  {
					  Word this_word = value.get(i);
					  for (Word ww:keywords)
					  {
						  if(ww.text.contains(this_word.text) || this_word.text.contains(ww.text) || ww.text.contains(this_word.lemma) )
						  {
							  System.out.println(this_word.text);
							  System.out.println(this_word.NETag);
							  System.out.println(this_word.POSTag);
							  String trimmed = this_word.text.trim();
							  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
							  if (words>1)
								  ww.lemma = this_word.lemma;
							  else
								  ww.lemma = ww.text;
							  ww.POSTag = this_word.POSTag;
							  of_4 = ww;
							  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
							  System.out.println(ww.text);
							  System.out.println("......");
							  flag=1;
							  break;
						  }
						  
					  }
					  if (flag==1)
					  {break;
					  }
				  }
				  for (int i = of_index_2-1; i > of_index_1; i--)
				  {
					  Word this_word = value.get(i);
					  for (Word ww:keywords)
					  {
						  if(ww.text.contains(this_word.text) || this_word.text.contains(ww.text) || ww.text.contains(this_word.lemma) )
						  {
							  System.out.println(this_word.text);
							  System.out.println(this_word.NETag);
							  System.out.println(this_word.POSTag);
							  String trimmed = this_word.text.trim();
							  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
							  if (words>1)
								  ww.lemma = this_word.lemma;
							  else
								  ww.lemma = ww.text;
							  ww.POSTag = this_word.POSTag;
							  of_3 = ww;
							  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
							  System.out.println(ww.text);
							  System.out.println(".......");
							  flag=1;
							  break;
						  }
						  
					  }
					  if (flag==1)
					  {break;
					  }
				  }
				  for (int i = of_index_1+1; i < of_index_2; i++)
				  {
					  Word this_word = value.get(i);
					  for (Word ww:keywords)
					  {
						  if(ww.text.contains(this_word.text) || this_word.text.contains(ww.text) || ww.text.contains(this_word.lemma) )
						  {
							  System.out.println(this_word.text);
							  System.out.println(this_word.NETag);
							  System.out.println(this_word.POSTag);
							  String trimmed = this_word.text.trim();
							  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
							  if (words>1)
								  ww.lemma = this_word.lemma;
							  else
								  ww.lemma = ww.text;
							  ww.POSTag = this_word.POSTag;
							  of_2 = ww;
							  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
							  System.out.println(ww.text);
							  System.out.println("........");
							  flag=1;
							  break;
						  }
						  
					  }
					  if (flag==1)
					  {break;
					  }
				  }
				  for (int i = of_index_1-1; i > 0; i--)
				  {
					  Word this_word = value.get(i);
					  for (Word ww:keywords)
					  {
						  if(ww.text.contains(this_word.text) || this_word.text.contains(ww.text) || ww.text.contains(this_word.lemma) )
						  {
							  System.out.println(this_word.text);
							  System.out.println(this_word.NETag);
							  System.out.println(this_word.POSTag);
							  String trimmed = this_word.text.trim();
							  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
							  if (words>1)
								  ww.lemma = this_word.lemma;
							  else
								  ww.lemma = ww.text;
							  ww.POSTag = this_word.POSTag;
							  of_1 = ww;
							  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
							  System.out.println(ww.text);
							  System.out.println("......");
							  flag=1;
							  break;
						  }
						  
					  }
					  if (flag==1)
					  {break;
					  }
				  }
				  }
				  else
				  {
					  for (int i = of_index_1+1; i < value.size(); i++)
					  {
						  Word this_word = value.get(i);
						  for (Word ww:keywords)
						  {
							  if(ww.text.contains(this_word.text) || this_word.text.contains(ww.text) || ww.text.contains(this_word.lemma) )
							  {		
								  System.out.println("Word of_2");
								  System.out.println(this_word.text);
								  System.out.println(this_word.NETag);
								  System.out.println(this_word.POSTag);
								  String trimmed = this_word.text.trim();
								  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
								  if (words>1)
									  ww.lemma = this_word.lemma;
								  else
									  ww.lemma = ww.text;
								  ww.POSTag = this_word.POSTag;
								  of_2 = ww;
								  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
								  System.out.println(ww.text);
								  System.out.println("........");
								  flag=1;
								  break;
							  }
							  
						  }
						  if (flag==1)
						  {break;
						  }
					  }
					  for (int i = of_index_1-1; i > 0; i--)
					  {
						  Word this_word = value.get(i);
						  for (Word ww:keywords)
						  {
							  if(ww.text.contains(this_word.text) || this_word.text.contains(ww.text) || ww.text.contains(this_word.lemma) )
							  {
								  System.out.println("Word of_1");
								  System.out.println(this_word.text);
								  System.out.println(this_word.NETag);
								  System.out.println(this_word.POSTag);
								  String trimmed = this_word.text.trim();
								  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
								  if (words>1)
									  ww.lemma = this_word.lemma;
								  else
									  ww.lemma = ww.text;
								  ww.POSTag = this_word.POSTag;
								  of_1 = ww;
								  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
								  System.out.println(ww.text);
								  System.out.println("......");
								  flag=1;
								  break;
							  }
							  
						  }
						  if (flag==1)
						  {break;
						  }
					  }  
				  }
				  
				  if (of_index_1>-1 && of_index_2<0)
				  {
					  if(of_2!=null)
					  {
					  PODNode pod2 = new PODNode(of_2.text, of_2.POSTag, of_2.lemma);
					  pods.add(pod2); // create PODNode and add
					  System.out.println("Adding of word : "+of_2.text);
					  }
					  if(of_1!=null)
					  {
					  PODNode pod3 = new PODNode(of_1.text, of_1.POSTag, of_1.lemma);
					  pods.add(pod3); // create PODNode and add
					  System.out.println("Adding of word : "+of_1.text);
					  }
				  }
				  else if (of_index_1>-1 && of_index_2>-1 && of_3 == of_2)
				  {
					  of_3 = of_4;	// 3 keywords in a row
					  if(of_3!=null)
					  {
					  PODNode pod1 = new PODNode(of_3.text, of_3.POSTag, of_3.lemma);
					  pods.add(pod1); // create PODNode and add
					  System.out.println("Adding of word : "+of_3.text);
					  }
					  if(of_2!=null)
					  {
					  PODNode pod2 = new PODNode(of_2.text, of_2.POSTag, of_2.lemma);
					  pods.add(pod2); // create PODNode and add
					  System.out.println("Adding of word : "+of_2.text);
					  }
					  if(of_1!=null)
					  {
					  PODNode pod3 = new PODNode(of_1.text, of_1.POSTag, of_1.lemma);
					  pods.add(pod3); // create PODNode and add
					  System.out.println("Adding of word : "+of_1.text);
					  }
				  }
				  
				  else if (of_index_1>-1 && of_index_2>-1) 
				  {
					  PODNode pod1 = new PODNode(of_4.text, of_4.POSTag, of_4.lemma);
					  pods.add(pod1); // create PODNode and add
					  System.out.println("Adding of word : "+of_4.text);
					  PODNode pod2 = new PODNode(of_3.text, of_3.POSTag, of_3.lemma);
					  pods.add(pod2); // create PODNode and add
					  System.out.println("Adding of word : "+of_3.text);
					  PODNode pod3 = new PODNode(of_2.text, of_2.POSTag, of_2.lemma);
					  pods.add(pod3); // create PODNode and add
					  System.out.println("Adding of word : "+of_2.text);
					  PODNode pod4 = new PODNode(of_1.text, of_1.POSTag, of_1.lemma);
					  pods.add(pod4); // create PODNode and add
					  System.out.println("Adding of word : "+of_1.text);
					  // 4 keywords in a row
				  }
				  // added to pods graph
				  
				  if (of_1!=null) // remove from keywords list
				  {
					  Iterator<Word> its = keywords.iterator();
					  while (its.hasNext()) {
					      Word w = its.next();
					      if (w.text.equals(of_1.text)) {
					      its.remove();
					      }
					  }
				  }
				  if (of_4!=null) // remove from keywords list
				  {
					  Iterator<Word> its = keywords.iterator();
					  while (its.hasNext()) {
					      Word w = its.next();
					      if (w.text.equals(of_4.text)) {
					      its.remove();
					      }
					  }
				  }
				  if (of_2!=null) // remove from keywords list
				  {
					  Iterator<Word> its = keywords.iterator();
					  while (its.hasNext()) {
					      Word w = its.next();
					      if (w.text.equals(of_2.text)) {
					      its.remove();
					      }
					  }
				  }
				  if (of_3!=null) // remove from keywords list
				  {
					  Iterator<Word> its = keywords.iterator();
					  while (its.hasNext()) {
					      Word w = its.next();
					      if (w.text.equals(of_3.text)) {
					      its.remove();
					      }
					  }
				  }
				  // deleted from keywords
			  }
			
			
			  System.out.println("Checking remaining words");
			  // if still keywords left, take all permutations
			  // actually just takes the remaining keywords in order
			  Iterator<Word> its = keywords.iterator();
			  while (its.hasNext()) {
			      Word w = its.next();
			      
			      for (Word ww:value)
				  {
					  if(w.text.contains(ww.text) || ww.text.contains(w.text) || w.text.contains(ww.lemma) )
					  {		
						  
						  System.out.println(ww.text);
						  System.out.println(ww.NETag);
						  System.out.println(ww.POSTag);
						  String trimmed = ww.text.trim();
						  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
						  if (words>1)
							  w.lemma = ww.lemma;
						  else
							  w.lemma = w.text;
						  w.POSTag = ww.POSTag;
						  
						  //candidate = ww; // replace the word by the corresponding keyword (i.e. expansion in a way)
						  System.out.println(ww.text);
						  System.out.println("........");
						  
					  }
					  
				  }
			      PODNode pod1 = new PODNode(w.text, w.POSTag, w.lemma);
			      
				  pods.add(pod1); // create PODNode and add
			      
			      }
			    if (which_word!=null)
			    {
			  	PODNode podd = new PODNode(which_word.text, which_word.POSTag, which_word.lemma);
			  	String trimmed = which_word.text.trim();
				  int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
				  if (words>1)
				  {
					  podd.lemma = which_word.lemma;
					  podd.POSTag = "NNP";
				  }
				  else
					  podd.lemma = podd.text;
				pods.add(podd); // create PODNode and add
			    }// added which
				ArrayList<ArrayList<PODNode>> pods_list = new ArrayList<ArrayList<PODNode>>();
				pods_list.add(pods);
				PODS_map.put(q.trim(),pods_list);
			  }
			  
		catch (Exception e) {
			e.printStackTrace();
			}
			  
			 
			  
		
		pods_for_this_q = pods;
		return pods_for_this_q;
					
		}
		
	public static void readCSV()
	{
		String csvFile = "test.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";
	 
		try {
	 
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
	 
			        // Assuming CSV files use semicolon as separator
				String[] questions = line.split(cvsSplitBy);
				
				String keywords = "";
				int indexOfOpenBracket = questions[1].indexOf("(");
				int indexOfLastBracket = questions[1].lastIndexOf(")");

				keywords = questions[1].substring(indexOfOpenBracket+1, indexOfLastBracket);
				String[] keyword_list = keywords.split(",");
				ArrayList<Word> QKeywords = new ArrayList<Word>(); // running out of appropriate variable names...
				for (String word : keyword_list)
				{
					String NE = "";
					if(Character.isUpperCase(word.trim().charAt(0)))
						NE = "NE";
					Word this_word = new Word (word.trim(), NE, "", ""); 
					QKeywords.add(this_word);
					
				}
				
				keywords_map.put(questions[0].trim(), QKeywords);
			}
				for (Entry<String, ArrayList<Word>> entry : keywords_map.entrySet()) {
					  String key = entry.getKey();
					  ArrayList<Word> value = entry.getValue();
					  System.out.println(key+" , ");
					  
					  for (int i = 0; i < value.size(); i++)
					    {
					        Word w = value.get(i);
					        System.out.println("Word : " + w.text + " , "+w.NETag+" , "+w.lemma+" , "+w.POSTag);
					    } 
					  // this loop just for printing to check if map is correctly created
					}
			
				csvFile = "test_words.csv";
				br = null;
				line = "";
				cvsSplitBy = ";";
				br = new BufferedReader(new FileReader(csvFile));
				while ((line = br.readLine()) != null) {
		 
				        // use semicolon as separator
					String[] words = line.split(cvsSplitBy);
					ArrayList<Word> word_details = new ArrayList<Word>();
					// just testing output
					//System.out.println(questions.length+" , "+questions[0]+ " , " + questions[1]+" , "+questions[2]);
				int list_length = words.length;
				for (int i=2; i<list_length;)
				{   Word this_word = new Word(words[i],words[i+1],words[i+2],words[i+3]);
					word_details.add(this_word);
					i=i+4;
				}
				
					words_map.put(words[1], word_details);
					
				}
					for (Entry<String, ArrayList<Word>> entry : words_map.entrySet()) {
						  String key = entry.getKey();
						  ArrayList<Word> value = entry.getValue();
						  System.out.println(key+" , ");
						  
						  for (int i = 0; i < value.size(); i++)
						    {
						        Word w = value.get(i);
						        // converting all capitalized letters to named entities, except if its the first word
						        if (Character.isUpperCase(w.text.charAt(0)) && i!=0 && (w.NETag.equals("O")))
						        {
						            w.NETag = "NE";
						        }
						        // if word is a NE and also JJ, designate it as not a NE
						        if ((w.POSTag.equals("JJ")) && !(w.NETag.equals("O")))
						        {
						            w.NETag = "O";
						        }
						        System.out.println("Word : " + w.text + " , "+w.NETag+" , "+w.lemma+" , "+w.POSTag);
						    } 
						// this loop for making changes to NEs and printing to check if map is correctly created
						}
			}
			
	 
		 catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		System.out.println("Done");
	}


	
	/* Rules that handle the YES/NO questions */
	public static ArrayList<PODNode> boolean_rules(String question) {
		
		ArrayList <PODNode> pods = new ArrayList<PODNode>();

				ArrayList<Word> keywords = keywords_map.get(question);
				System.out.println("keywords" + keywords);
				boolean NEFound = false;
				ArrayList<Word> NEs = new ArrayList<Word>();
				/* Is <Noun> <Prep> <Adj> ?
				 * Make NE as the head of the PODS
				 */
					ArrayList<Word> tagged_words = words_map.get(question);
					tagged_words.remove(0);
					for(Word word : keywords) {
						//Get the keyword from tagged list and check the tag of the keyword
						System.out.println("word : " + word);
						Word kw = null;
						System.out.println("tagged words : " + tagged_words);
						for(Word wd : tagged_words) {
							if(word.text.toLowerCase().contains(wd.text.toLowerCase()) || wd.text.toLowerCase().contains(word.text.toLowerCase()) || word.text.toLowerCase().contains(wd.lemma.toLowerCase())) {
								String key = word.text;
								kw = wd;
								kw.text = key;
								int ind = tagged_words.indexOf(wd);
								tagged_words.remove(ind);
								System.out.println(" Matched word " + kw);
								break;
							}
						}
						if(kw != null) {
							//If the keyword is entity then put it at the first location in the PODS list
							//if(!(kw.NETag.equals("O")) && !(kw.NETag.equals("MISC"))) {
							  if(word.NETag == "NE") {
								if(NEFound == false) {
									pods.add(0, new PODNode(kw.text, kw.POSTag, kw.lemma));
									NEFound = true;
								}
								else {
									NEs.add(kw);
									/*try {
										Word pivot = DBPediaInterface.getPivot(NEs);
										pods.add(0, new PODNode(pivot.text, pivot.POSTag, pivot.lemma));
										NEs.remove(pivot);
									}
									catch(Exception e) {
										System.out.println("DBPedia server is down");
									}*/
								}
							}
							else {
								pods.add(new PODNode(kw.text, kw.POSTag, kw.lemma));
							}
						}
						else {  //The keyword is not present in the question
							    
							if(word.POSTag.startsWith("N")) {
								pods.add(1, new PODNode(word.text, word.POSTag, word.lemma));
							}
							else {
								System.out.println("This case should not occur. As usually the entity which is not present in the question is noun. We do not need hints for non noun terms.");
							}
						}
						
					}//The for loop for keywords ends
					
					//Add the remaining NEs in the end
					for(Word word : NEs) {
						pods.add(new PODNode(word.text, word.POSTag, word.lemma));
					}
					
					System.out.println("\n\nPODS for question " + question );
					for(PODNode p : pods) {
						System.out.print(" " + p.text);
					}
					System.out.println("\n\n");
		return pods;
		
	}
	
	
	public static ArrayList<PODNode> create_pods(Question q) {
		
		ArrayList<PODNode> pods = new ArrayList<PODNode>();
		if(q.questionType == QuestionType.BOOLEAN) {
			pods = PodsGeneration.boolean_rules(q.text.trim());
		}
		else {
			pods = PodsGeneration.nonboolean_rules(q.text);
		}
		
		return pods;
	}
	
}