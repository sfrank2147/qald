import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import jobimApi.Usage;

import java.util.HashMap;
import java.util.Map.Entry;

public class Algorithm {
	
	public enum JobimSource {NO_MATCH, PREDICATE,NOUN,PREDICATE_AS_NOUN};
	
	public static ArrayList<String> answerBinaryQuestion(ArrayList<PODNode> podList) {
		ArrayList<String> POD = new ArrayList<String>();
		for(PODNode podNode : podList){
			POD.add(podNode.text);
		}
		return answerBinaryQuestion(POD);
	}
	
	
	/**
	 * Function to answer a binary, i.e. yes/no question
	 * Returns an arraylist of strings for compatibility with other functions
	 * Returns either "yes" or "no"
	 * @param dependencyList
	 * @return
	 */
	public static ArrayList<String> answerBinaryQuestion(List<String> dependencyList) {
		int dependencyLength = dependencyList.size();
		String currentResource = "";
		int currentNode = 0;
		
		ArrayList<String> answers = new ArrayList<String>();
		
		//stop when we get to the final node in the graph.  the final node is what we're comparing our answer to
		while(currentNode < dependencyLength - 1) {
			
			//if we're at the beginning of the list, we need to get a starting resource
			if(currentNode == 0) {
				String keyword = dependencyList.get(currentNode);
				
				//get the best dbpedia resource for the current node
				ArrayList<String> resourceList = DBPediaInterface.getResourceURLs(keyword);
				//if we can't match a resource, we're screwed
				if(resourceList.size() == 0) {
					return null;
				} else {
					currentResource = resourceList.get(0);
				}
				currentNode = currentNode + 1;
			}
			
			//get all the relationships for that resources
			//the resource could be either a subject or an object
			ArrayList<Triple> currentRelationshipTriples = DBPediaInterface.getRelationships(currentResource);
			
			//the relationship is the next word in the list
			//e.g., if the list is [Obama wife graduate university] and the current word is Obama,
			//then the relationship is wife
			String relationship = dependencyList.get(currentNode);
			
			//update the current resource using the possible matches from jobimtext
			currentResource = updateResource(currentResource, currentRelationshipTriples, relationship);
			
			//if we hit a dead end, return no
			if(currentResource == null || currentResource.equals("")) {
				answers.add("no");
				return answers;
			}
			
			currentNode += 1; 
		}
		//the last entry in the dependency list is what we should see if the answer is yes
		String expectedResult = dependencyList.get(dependencyLength - 1);
		if(
				currentResource.toLowerCase().contains(expectedResult.toLowerCase()) || 
				currentResource.equals(DBPediaInterface.getResourceURLs(expectedResult).get(0))
				) {
			answers.add("yes");
		} else {
			answers.add("no");
		}
		
		
		return answers;
	}
	
	public static ArrayList<String> answerQuestionFromDependencyList(ArrayList<PODNode> podList) {
		ArrayList<String> POD = new ArrayList<String>();
		for(PODNode podNode : podList){
			POD.add(podNode.text);
		}
		return answerQuestionFromDependencyList(POD);
	}

	/**
	 * This is the big kahuna!
	 * Input: the dependency list, passed in as a list
	 * Output: an arraylist of the dbpedia resources we identify as answers
	 * @param dependencyList
	 * @return
	 */
	
	
	public static ArrayList<String> answerQuestionFromDependencyList(List<String> dependencyList) {
		int dependencyLength = dependencyList.size();
		String currentResource = "";
		int currentNode = 0;
		
		ArrayList<String> answers = new ArrayList<String>();
		
		//stop when we get to the final node in the graph
		while(currentNode < dependencyLength - 1) {
			
			//if we're at the beginning of the list, we need to get a starting resource
			if(currentNode == 0) {
				String keyword = dependencyList.get(currentNode);
				
				//get the best dbpedia resource for the current node
				ArrayList<String> resourceList = DBPediaInterface.getResourceURLs(keyword);
				//if we can't match a resource, we're screwed
				if(resourceList.size() == 0) {
					return null;
				} else {
					currentResource = resourceList.get(0);
				}
				currentNode = currentNode + 1;
			}
			
			//get all the relationships for that resources
			//the resource could be either a subject or an object
			ArrayList<Triple> currentRelationshipTriples = DBPediaInterface.getRelationships(currentResource);
			
			//the relationship is the next word in the list
			//e.g., if the list is [Obama wife graduate university] and the current word is Obama,
			//then the relationship is wife
			String relationship = dependencyList.get(currentNode);
			
			//update the current resource using the possible matches from jobimtext
			String nextResource = updateResource(currentResource, currentRelationshipTriples, relationship);
			if(nextResource == null || nextResource.equals("")) {
				answers.add(currentResource);
				return answers;
			} else {
				currentResource = nextResource;
			}
			
			currentNode += 1; 
		}
		answers.add(currentResource);
		return answers;
	}

	
	
	/**
	 * Algorithm, but adapted for lists
	 */
	
	public static ArrayList<String> answerListQuestion(ArrayList<PODNode> podList) {
		ArrayList<String> POD = new ArrayList<String>();
		for(PODNode podNode : podList){
			POD.add(podNode.text);
		}
		return answerListQuestion(POD);	}
	
	public static ArrayList<String> answerListQuestion(List<String> dependencyList) {
		
		
		int dependencyLength = dependencyList.size();
		int currentNode = 0;
		
		//for a list, at any given time we'll have multiple resources
		HashSet<String> currentResources = new HashSet<String>();
		
		while(currentNode < dependencyLength) {
			//initialize the first resource
			//if we're at the beginning of the list, we need to get a starting resource
			if(currentNode == 0) {
				String keyword = dependencyList.get(currentNode);
				
				//get the best dbpedia resource for the current node
				ArrayList<String> resourceList = DBPediaInterface.getResourceURLs(keyword);
				//if we can't match a resource, we're screwed
				if(resourceList.size() == 0) {
					return null;
				} else {
					currentResources.add(resourceList.get(0));
				}
				currentNode = currentNode + 1;
			}
			
			//at each iteration, update the resource set using ALL of our current resources
			HashSet<String> nextResourceSet = new HashSet<String>();
			
			//the next relationship is the same for each of the resources in our set
			String relationship = dependencyList.get(currentNode);

			for (String resource : currentResources) {
				ArrayList<Triple> currentRelationshipTriples = DBPediaInterface.getRelationships(resource);
				for(String nextResource : updateResourceForList(resource, currentRelationshipTriples, relationship)) {
					nextResourceSet.add(nextResource);
				}
			}
			
			//if the next resource set is empty, return our current one
			//if it's empty, it might actually just contain the empty string
			if(isEmpty(nextResourceSet)) {
				return new ArrayList<String>(currentResources);
			} else {
				currentResources = nextResourceSet;
			}
			currentNode += 1;
		}

		return new ArrayList<String>(currentResources);
				
	}
	
	static String updateResource(String currentResource, ArrayList<Triple> relationshipTriples, String rawRelationship) {
		//strategy: for each predicate, find the jobim word that matches it best
		//take that score, and choose the predicate with the best score
		//then return the subject/object that's not the current resource
		
		//the ideal case is if the raw realtionship is one of the dbpedia relationships
		//maybe have to stem it
		String bestResource = findStemmedMatch(currentResource, rawRelationship, relationshipTriples);
		if(bestResource != null && bestResource.length() > 0) {
			return bestResource;
		}
		
		bestResource = findCamelCaseMatch(currentResource, rawRelationship, relationshipTriples);
		if(bestResource != null && bestResource.length() > 0) {
			return bestResource;
		}
		
		bestResource = findMultiWordMatch(currentResource, rawRelationship, relationshipTriples);
		if(bestResource != null && bestResource.length() > 0) {
			return bestResource;
		}
		
		
		
		
		int bestScore = 0;
		HashMap<String,Integer> jobimMap = Usage.getSimilarTerms(rawRelationship,"VB");
		//jobim match keys all end in #something, we don't want that

		
		for (Triple triple : relationshipTriples) {
			String predicate = triple.predicate;
			//only want the english word portion
			predicate = DBPediaInterface.shortenResource(predicate);
			if(predicate.equals("creator")) {
				System.out.println("hi");
			}
			//find the jobimtext word that matches it best
			String jobimMatch = findJobimMatch(predicate,jobimMap,2);
			if(jobimMatch == null) {
				continue;
			}
			int score = jobimMap.get(jobimMatch).intValue();
			if(score > bestScore) {
				bestScore = score;
				//get the resource that isn't the currentResource
				String shortenedCurrentResource = DBPediaInterface.shortenResource(currentResource);
				String shortenedSubject = DBPediaInterface.shortenResource(triple.subject);
				bestResource = shortenedCurrentResource.equals(shortenedSubject) ? triple.object : triple.subject;
			}
		}
		
		//now, try using the subject or object as the relationship
		jobimMap = Usage.getSimilarTerms(rawRelationship,"NN");
			//jobim match keys all end in #something, we don't want that

			
			for (Triple triple : relationshipTriples) {
				//this time, we're checking nouns, not verbs
				//get the noun that isn't the currentResource
				String shortenedCurrentResource = DBPediaInterface.shortenResource(currentResource);
				String shortenedSubject = DBPediaInterface.shortenResource(triple.subject);
				String noun = shortenedCurrentResource.equals(shortenedSubject) ? DBPediaInterface.shortenResource(triple.object) : shortenedSubject;
				
				if(noun.equals("spouse")) {
					System.out.println("hi");
				}

				//find the jobimtext word that matches it best
				String jobimMatch = findJobimMatch(noun,jobimMap,2);
				
				//prevent blank words from matching
				if(jobimMatch == null || jobimMatch.length() < 3) {
					continue;
				}
				int score = jobimMap.get(jobimMatch).intValue();
				if(score > bestScore) {
					bestScore = score;
					//get the resource that isn't the currentResource
					shortenedSubject = DBPediaInterface.shortenResource(triple.subject);
					bestResource = shortenedCurrentResource.equals(shortenedSubject) ? triple.object : triple.subject;
				}
			}
		
		//now, try using the predicate as a noun
			
			for (Triple triple : relationshipTriples) {
				//this time, we're checking nouns, not verbs
				//get the noun that isn't the currentResource
				String noun = DBPediaInterface.shortenResource(triple.predicate);

				//find the jobimtext word that matches it best
				String jobimMatch = findJobimMatch(noun,jobimMap,2);
				
				//prevent blank words from matching
				if(jobimMatch == null || jobimMatch.length() < 3) {
					continue;
				}
				int score = jobimMap.get(jobimMatch).intValue();
				if(score > bestScore) {
					bestScore = score;
					//get the resource that isn't the currentResource
					String shortenedSubject = DBPediaInterface.shortenResource(triple.subject);
					bestResource = DBPediaInterface.shortenResource(currentResource).equals(shortenedSubject) ? triple.object : triple.subject;
				}
			}
//		
//		//if none of this worked, use the stemmer to see if we can find a perfect stemmed match
//		if(bestResource.equals("")) {
//			bestResource = findStemmedMatch(currentResource, rawRelationship, relationshipTriples);
//		}
		
		return bestResource;
	}
	
	static HashSet<String> updateResourceForList(String currentResource, ArrayList<Triple> relationshipTriples, String rawRelationship) {
		//strategy: for each predicate, find the jobim word that matches it best
		//take that score, and choose the predicate with the best score
		//then return the subject/object that's not the current resource

		JobimSource source = JobimSource.NO_MATCH;

		HashSet<String> matchingResources = new HashSet<String>();

		int bestScore = 0;
		HashMap<String,Integer> jobimMap = Usage.getSimilarTerms(rawRelationship,"VB");
		//jobim match keys all end in #something, we don't want that

		//first, try to find the best predicate
		String bestPredicate = "";
		String bestResource = "";
		String bestNounPredicate = "";
		for (Triple triple : relationshipTriples) {
			String predicate = triple.predicate;
			//only want the english word portion
			predicate = DBPediaInterface.shortenResource(predicate);

			//find the jobimtext word that matches it best
			String jobimMatch = findJobimMatch(predicate,jobimMap,2);
			if(jobimMatch == null) {
				continue;
			}
			int score = jobimMap.get(jobimMatch).intValue();
			if(score > bestScore) {
				bestScore = score;
				//get the resource that isn't the currentResource
				bestPredicate = predicate;
				//				String shortenedCurrentResource = DBPediaInterface.shortenResource(currentResource);
				//				String shortenedSubject = DBPediaInterface.shortenResource(triple.subject);
				//				bestResource = shortenedCurrentResource.equals(shortenedSubject) ? triple.object : triple.subject;

				source = JobimSource.PREDICATE;
			}
		}

		//if verb didn't work, try noun
		//in this case, we're only adding one resource
		jobimMap = Usage.getSimilarTerms(rawRelationship,"NN");
		//jobim match keys all end in #something, we don't want that


		for (Triple triple : relationshipTriples) {
			//this time, we're checking nouns, not verbs
			//get the noun that isn't the currentResource
			String shortenedCurrentResource = DBPediaInterface.shortenResource(currentResource);
			String shortenedSubject = DBPediaInterface.shortenResource(triple.subject);
			String noun = shortenedCurrentResource.equals(shortenedSubject) ? DBPediaInterface.shortenResource(triple.object) : shortenedSubject;

			//find the jobimtext word that matches it best
			String jobimMatch = findJobimMatch(noun,jobimMap,2);

			//prevent blank words from matching
			if(jobimMatch == null || jobimMatch.length() < 3) {
				continue;
			}
			int score = jobimMap.get(jobimMatch).intValue();
			if(score > bestScore) {
				bestScore = score;
				//get the resource that isn't the currentResource
				shortenedSubject = DBPediaInterface.shortenResource(triple.subject);
				bestResource = shortenedCurrentResource.equals(shortenedSubject) ? triple.object : triple.subject;
				source = JobimSource.NOUN;
			}
		}

		//if that still fails, try using the predicate as a noun
		for (Triple triple : relationshipTriples) {
			//this time, we're checking nouns, not verbs
			//get the noun that isn't the currentResource
			//we could still have multiple relationships
			String noun = DBPediaInterface.shortenResource(triple.predicate);

			//find the jobimtext word that matches it best
			String jobimMatch = findJobimMatch(noun,jobimMap,2);

			//prevent blank words from matching
			if(jobimMatch == null || jobimMatch.length() < 3) {
				continue;
			}
			int score = jobimMap.get(jobimMatch).intValue();
			if(score > bestScore) {
				bestScore = score;
				bestNounPredicate = triple.predicate;
				source = JobimSource.PREDICATE_AS_NOUN;
			}
		}


		if(source == JobimSource.PREDICATE) {
			//if there's a match, add EVERY resource to matchingResources
			for (Triple relationshipTriple : relationshipTriples) {
				if(relationshipTriple.predicate.equals(bestPredicate)) {
					String shortenedCurrentResource = DBPediaInterface.shortenResource(currentResource);
					String shortenedSubject = DBPediaInterface.shortenResource(relationshipTriple.subject);
					matchingResources.add(shortenedCurrentResource.equals(shortenedSubject) ? relationshipTriple.object : relationshipTriple.subject);		
				}
			}
		} else if(source == JobimSource.NOUN) {
			matchingResources.add(bestResource);
		} else if(source == JobimSource.PREDICATE_AS_NOUN) {
			for (Triple relTriple : relationshipTriples) {
				if(relTriple.predicate.equals(bestNounPredicate)) {
					String shortenedSubject = DBPediaInterface.shortenResource(relTriple.subject);
					String resource = DBPediaInterface.shortenResource(currentResource).equals(shortenedSubject) ? relTriple.object : relTriple.subject;
					matchingResources.add(resource);
				}
			}
		}
		//if none of this worked, use the stemmer to see if we can find a perfect stemmed match
		if(matchingResources.size() == 0) {
			String stemmedMatch = findStemmedMatch(currentResource, rawRelationship, relationshipTriples);
			if(stemmedMatch != null && !stemmedMatch.equals("")) {
				matchingResources.add(stemmedMatch);
			}
		}
		if(matchingResources.size() == 0) {
			String camelMatch = findCamelCaseMatch(currentResource, rawRelationship, relationshipTriples);
			if(camelMatch != null && !camelMatch.equals("")) {
				matchingResources.add(camelMatch);
			}
		}
		if(matchingResources.size() == 0) {
			matchingResources.add(findMultiWordMatch(currentResource, rawRelationship, relationshipTriples));		
		}

		
		return matchingResources;
	}
	
	//find the word in the jobimText hashmap with the best levenshtein distance to word
	//returns null if the best match is more than maxDistance apart
	static String findJobimMatch(String word, HashMap<String,Integer> jobimMap, int maxDistance) {
		word = word.toLowerCase();
		
		String bestMatch = null;
		int bestDistance = maxDistance + 1;
		
		//check for exact matches
		if(jobimMap.containsKey(word)) {
			return word;
		} else {
			Set<String> jobimWords = jobimMap.keySet();

			for(String jobimWord : jobimWords) {
				if(
						//hackish comparison
				  ((jobimWord.startsWith(word) || word.startsWith(jobimWord))
				  && (Math.abs(jobimWord.length() - word.length()) < 3)
				  && ((float)jobimWord.length()/(float)word.length() > 0.3)
				  && ((float)jobimWord.length()/(float)word.length() < 3))
				  ||
				  //using porter stemmer
				  compareRoots(jobimWord,word)
								) {
					return jobimWord;
				}

			}
			return null;
		}
	}
	
	//function to compare x and y after applying the porter stemmer
	public static boolean compareRoots(String x, String y) {
//		return stemWord(x.toLowerCase()).equals(stemWord(y.toLowerCase()));
		return stemWord(x.toLowerCase()).equals(stemWord(y.toLowerCase()));
	}
	
	public static String stemWord(String word) {
		Stemmer stemmer = new Stemmer();
		for(char c : word.toCharArray()) {
			stemmer.add(c);
		}
		stemmer.stem();
		return stemmer.toString();
	}
	
	/**
	 * Lookds through the relationship triples, looking for a resource that, when stemmed, matches the raw relationship
	 * returns the appropriate resource (that's not the current resource)
	 * @return
	 */
	public static String findStemmedMatch(String currentResource, String rawRelationship, ArrayList<Triple> relationshipTriples) {
		String match = "";
		String stemmedRawRelationship = stemWord(rawRelationship);
		
		for(Triple triple : relationshipTriples) {
			String subj = DBPediaInterface.shortenResource(triple.subject);
			String pred = DBPediaInterface.shortenResource(triple.predicate);
			String obj = DBPediaInterface.shortenResource(triple.object);
			if(stemWord(subj).equals(stemmedRawRelationship)) {
				match = triple.subject;
			}
			else if(stemWord(obj).equals(stemmedRawRelationship)) {
				match = triple.object;
			}
			else if(stemWord(pred).equals(stemmedRawRelationship)) {
				match = (triple.subject.equals(currentResource) ? triple.object : triple.subject);
			}
		}
		
		return match;
	}
	
	//assuming that rawRelationship is >= two words
	public static String findMultiWordMatch(String currentResource, String rawRelationship, ArrayList<Triple> relationshipTriples) {
		String[] words = rawRelationship.split(" ");
		String resource = "";
		for(String word : words){
			resource = findStemmedMatch(currentResource, word, relationshipTriples);
			if(resource.length() == 0) {
				resource = findCamelCaseMatch(currentResource, word, relationshipTriples);
			}
			if(resource.length() > 0) {
				return resource;
			}
		}
		return resource;
	}
	
	//searches for matches that come from camel case
	public static String findCamelCaseMatch(String currentResource, String rawRelationship, ArrayList<Triple> relationshipTriples) {
		String match = "";
		rawRelationship = rawRelationship.toLowerCase();
		
		for(Triple triple : relationshipTriples) {
			String subj = DBPediaInterface.shortenResource(triple.subject);
			String pred = DBPediaInterface.shortenResource(triple.predicate);
			String obj = DBPediaInterface.shortenResource(triple.object);
			if(splitCamelCase(subj).equals(rawRelationship)) {
				match = triple.subject;
			}
			else if(splitCamelCase(obj).equals(rawRelationship)) {
				match = triple.object;
			}
			else if(splitCamelCase(pred).equals(rawRelationship)) {
				match = (triple.subject.equals(currentResource) ? triple.object : triple.subject);
			}
		}
		
		return match;
	}
	/**
	 * Splits a camelcase word into multiple words
	 * For example, "areaTotal" becomes "area total" 
	 * @param word
	 * @return
	 */
	public static String splitCamelCase(String word) {
		char wordChars[] = word.toCharArray();
		int x = 1;
		while(x < wordChars.length) {
			if(isUpperCase(wordChars[x]) && isLowerCase(wordChars[x-1])) {
				word = word.substring(0, x) + " " + word.substring(x);
				wordChars = word.toCharArray();
				x++;
			}
			x++;
		}
		
		return word.toLowerCase();
	}
	
	private static boolean isLowerCase(char letter) {
		return (97 <= letter && letter <= 122);
	}
	
	private static boolean isUpperCase(char letter) {
		return(65 <= letter && letter <= 90);
	}
	
	//methods for checking if a hash set or array list of strings is empty
	private static boolean isEmpty(Iterable<String> stringSet) {
		boolean empty = true;
		for(String string : stringSet){
			if(!string.equals("")) {
				empty = false;
				break;
			}
		}
		return empty;
	}

	
}
