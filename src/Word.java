public class Word {
		String text; 
		String POSTag;
		String lemma; // lemmatized
		String NETag; // named entity tag
		public Word()
		{
		}
		
		public Word(String t, String ne, String pos, String lem )
		{
			this.text=t;
			this.POSTag=pos;
			this.lemma=lem;
			this. NETag=ne;
		}
		
		@Override
		public String toString() {
			String result = "(" + text + "," + POSTag + "," + lemma + "," + NETag + ")";
			return result;
		}
		
		@Override
		public boolean equals(Object o) {
			boolean res = false;
			if(o instanceof Word) {
				res = ((Word) o).text == this.text;
			}
			
			return res;
		}
}


