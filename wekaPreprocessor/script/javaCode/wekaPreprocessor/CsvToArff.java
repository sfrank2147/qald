package wekaPreprocessor;

import java.io.File;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

public class CsvToArff {
	  public static void main(String[] args) throws Exception {
		    if (args.length != 2) {
		      System.out.println("\nUsage: CsvToArff <input.csv> <output.arff>\n");
		      System.exit(1);
		    }
		 
		    // load CSV
		    CSVLoader loader = new CSVLoader();
//		    loader.setSource(new File("ListTest.csv"));
		    loader.setSource(new File(args[0]));
		    Instances data = loader.getDataSet();
		 
		    // save ARFF
		    ArffSaver saver = new ArffSaver();
		    saver.setInstances(data);
//		    saver.setFile(new File("List.arff"));
	//	    saver.setDestination(new File("List.arff"));
		    saver.setFile(new File(args[1]));
		    saver.setDestination(new File(args[1]));
		    saver.writeBatch();
		  }
}
