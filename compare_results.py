import sys
import pdb

test_file_path = sys.argv[1]
correct_path = sys.argv[2]

test_file = open(test_file_path,'r')
correct_file = open(correct_path,'r')

test_answers = test_file.readlines()
correct_answers = correct_file.readlines()

num_answers = min(len(test_answers),len(correct_answers))

for x in range(num_answers):
    test_answer_list = test_answers[x].split(',')
    correct_answer_list = correct_answers[x].split(',')
    count = 0

    #clean answers
    test_answer_list = [y[:-1] if y[-1] == '\n' else y for y in test_answer_list ]
    correct_answer_list = [y[:-1]  if y[-1] == '\n' else y for y in correct_answer_list]
    for answer in test_answer_list:
        answer = answer.replace('en.wikipedia.org/wiki//wiki','http://dbpedia.org/resource')
	if answer in correct_answer_list:
	    count += 1
    if(count > 0):
	print('{0}: {1} out of {2}'.format(x, count, len(correct_answer_list)))
test_file.close()
correct_file.close()
