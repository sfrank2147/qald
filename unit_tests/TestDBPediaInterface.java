import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;

public class TestDBPediaInterface {

	@Test
	public void testGetResourceFromKeyword() {
		ArrayList<String> resourceURLs = DBPediaInterface.getResourceURLs("Barack Obama");
		assertEquals("http://dbpedia.org/resource/Barack_Obama", resourceURLs.get(0));
		assertEquals("http://dbpedia.org/resource/Presidency_of_Barack_Obama", resourceURLs.get(1));
		assertEquals(resourceURLs.size(),5);
	}
	
	@Test
	public void testGetRelationships() {
		ArrayList<Triple> relationships = DBPediaInterface.getRelationships("http://dbpedia.org/resource/Ignatius_Isaac_Azar");
		assertEquals(relationships.get(45).subject, "http://dbpedia.org/resource/Ignatius_Isaac_Azar");
		assertEquals(relationships.get(45).subjectLiteral, false);
		assertEquals(relationships.get(45).predicate,"http://dbpedia.org/ontology/wikiPageOutLinkCount");
		assertEquals(relationships.get(45).predicateLiteral, false);
		assertEquals(relationships.get(45).object,"15");
		assertEquals(relationships.get(45).objectLiteral,true);
		
		//check that the other direction also went through
		assertEquals(relationships.get(46).object, "http://dbpedia.org/resource/Ignatius_Isaac_Azar");
	}
	
	@Test
	public void testQueryLocation() {
		ArrayList<String> locations = DBPediaInterface.queryLocation(
				"http://dbpedia.org/resource/LaGuardia_Airport");
		assertEquals(locations.get(0),"http://dbpedia.org/resource/New_York_metropolitan_area");
		
	}
	
	@Test
	public void testFilterResults() {
		ArrayList<String> startingResources = new ArrayList<String>();
		startingResources.add("https://dbpedia.org/resource/LaGuardia_Airport");
		startingResources.add("https://dbpedia.org/resource/Miami_International_Airport");
		
		ArrayList<String> filteringResources = new ArrayList<String>();
		filteringResources.add("https://dbpedia.org/resource/New_York");
		
		ArrayList<String> results = DBPediaInterface.filterResults(startingResources, filteringResources);
		assertTrue(results.get(0).equals("http://dbpedia.org/resource/LaGuardia_Airport"));
		assertEquals(results.size(),1);
	}
	
	@Test
	public void testGetPivot() {
		ArrayList<Word> keywords = new ArrayList<Word>();
		keywords.add(new Word("Barack Obama","","",""));
		keywords.add(new Word("Katsumi_Toriumi","","",""));
		Word bestPivot = DBPediaInterface.getPivot(keywords);
		assertEquals(bestPivot, "Barack Obama");
	}
}
